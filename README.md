# Simple Nodejs App

## Description

Cette application est un exemple simple d'une application Nodejs avec un fond vert ou bleu. L'application est conteneurisée à l'aide de Docker et écoute sur le port 8080.

## Instructions

### Pré-requis

- Docker doit être installé sur votre machine.

### Construire l'image Docker

Pour construire l'image Docker, exécutez la commande suivante dans le répertoire du projet :

```bash
docker build -t nodejs-bluegreen-app .
```

### Exécuter le conteneur

Pour exécuter un conteneur basé sur l'image Docker construite, utilisez la commande suivante :

```bash
docker run -d -p 8080:8080 nodejs-bluegreen-app
```

### Accéder à l'application

Ouvrez un navigateur web et accédez à http://localhost:8080.

### Envoyer sur le Docker Hub

```bash
docker build -t nodejs-bluegreen-app .

docker login

docker tag nodejs-bluegreen-app username/nodejs-bluegreen-app:v-blue

docker push username/nodejs-bluegreen-app:v-blue
```
